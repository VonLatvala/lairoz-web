# LairOZ Web
[![Build Status](https://jenkins.alatvala.fi/buildStatus/icon?job=VonLatvala%2Flairoz-web%2Fmaster)](https://jenkins.alatvala.fi/job/VonLatvala/job/lairoz-web/job/master/)
[![Quality Gate](https://sonar.alatvala.fi/api/badges/gate?key=lairoz%3Alairoz-web)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Lines](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=lines)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Percentage of comments](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=comment_lines_density)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Complexity to function](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=function_complexity)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Test errors](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=test_errors)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Test failures](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=test_failures)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Test success density](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=test_success_density)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Unti Test Coverage](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=coverage)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Duplicated Lines Density](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=duplicated_lines_density)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Blocker Violations](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=blocker_violations)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Critical Violations](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=critical_violations)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Code Smells](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=code_smells)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Bugs](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=bugs)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Vulnerabilities](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=vulnerabilities)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![Technical Debt Ratio](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=sqale_debt_ratio)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![New Maintainability Rating](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=new_maintainability_rating)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![New Reliability Rating](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=new_reliability_rating)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)
[![New Security Rating](https://sonar.alatvala.fi/api/badges/measure?key=lairoz%3Alairoz-web&metric=new_security_rating)](https://sonar.alatvala.fi/dashboard/index/lairoz%3Alairoz-web)

## What is this

Web client for LairOZ

## How do I get set up?

These steps primarily apply to a non-Windows platform, but it is absolutely possible developing on Windows too.

* Have [yarn installed](https://yarnpkg.com/lang/en/docs/install).
* Have [git installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
* (Optional, but highly recommended) Configure your editor of choice to use [ESLint](https://eslint.org/docs/user-guide/getting-started).
    * For ([Neo](https://github.com/neovim/neovim/wiki/Installing-Neovim))[Vim](https://www.vim.org/download.php) (non-neo >=8), using [Worp/Ale](https://github.com/w0rp/ale#3-installation) is highly recommended. For first time vim users, using this in conjunction with [Vim Bootstrap](https://vim-bootstrap.com/) might be a good start.
    * For [Visual Studio Code](https://code.visualstudio.com/Download), using [ESLint from the marketplace](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) is highly recommended.
    * For emacs users, `> using emacs`
* Clone this repository i.e.

```sh
git clone https://VonLatvala@bitbucket.org/VonLatvala/lairoz-web.git
```

* cd into the cloned folder i.e.

```sh
cd lairoz-web
```

* Install project dependencies i.e.

```sh
yarn install
```

* Start the appliaction

```sh
yarn start
```
