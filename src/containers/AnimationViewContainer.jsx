import { connect } from 'react-redux'
import AnimationView from '../components/AnimationView'
import {
    setAnimatedViewSliders,
    setAnimatedFromTo,
} from '../actions/' // eslint-disable-line

const mapDispatchToProps = (dispatch, ownProps) => { // eslint-disable-line
    return {
        onFrom: {
            onHueSliderChange: (val, oldFromTo, speed) => {
                dispatch(
                    setAnimatedViewSliders({
                        ...oldFromTo.from, h: val
                    }, oldFromTo.to,
                    speed))
            },
            onSaturationSliderChange: (val, oldFromTo, speed) => {
                dispatch(
                    setAnimatedViewSliders({
                        ...oldFromTo.from, s: val
                    }, oldFromTo.to,
                    speed))
            },
            onValueSliderChange: (val, oldFromTo, speed) => {
                dispatch(
                    setAnimatedViewSliders({
                        ...oldFromTo.from, v: val
                    }, oldFromTo.to,
                    speed))
            }
        },
        onTo: {
            onHueSliderChange: (val, oldFromTo, speed) => {
                dispatch(
                    setAnimatedViewSliders(
                        oldFromTo.from, {
                            ...oldFromTo.to,
                            h: val
                        },
                        speed))
            },
            onSaturationSliderChange: (val, oldFromTo, speed) => {
                dispatch(
                    setAnimatedViewSliders(
                        oldFromTo.from, {
                            ...oldFromTo.to,
                            s: val
                        },
                        speed))
            },
            onValueSliderChange: (val, oldFromTo, speed) => {
                dispatch(
                    setAnimatedViewSliders(
                        oldFromTo.from, {
                            ...oldFromTo.to,
                            v: val
                        },
                        speed))
            }
        },
        onSpeedChange: (speed, oldFromTo) => {
            dispatch(
                setAnimatedViewSliders(
                    oldFromTo.from,
                    oldFromTo.to,
                    speed
                )
            )
        },
        onCommitAnimation: (fromTo, speed) => {
            dispatch(
                setAnimatedFromTo(fromTo, speed)
            )
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    const res = {
        ...ownProps,
        servers: state.servers,
        fromTo: {
            from: state.animatedConfig.fromTo.from,
            to: state.animatedConfig.fromTo.to,
        },
        speed: state.animatedConfig.speed
    }
    return res
}

export default connect(mapStateToProps, mapDispatchToProps)(AnimationView)
