import { connect } from 'react-redux'
import { updateServer, removeServer } from '../actions/index'
import ServerConfigRow from '../components/ServerConfigRow'

const mapDispatchToProps = dispatch => {
    return {
        updateServer: (serverId, server) => dispatch(updateServer(serverId, server)),
        deleteServer: serverId => dispatch(removeServer(serverId))
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        server: ownProps.server

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ServerConfigRow)
