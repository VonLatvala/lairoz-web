import { connect } from 'react-redux'
import { setView } from '../actions/index'
import AppNav from '../components/AppNav'

const mapDispatchToProps = dispatch => {
    return {
        setView: viewId => dispatch(setView(viewId))
    }
}

const mapStateToProps = (state, ownProps) => {
    const res = {
        ...ownProps,
        activeView: state.activeView,
    }
    return res
}

export default connect(mapStateToProps, mapDispatchToProps)(AppNav)
