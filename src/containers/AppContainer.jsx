import { connect } from 'react-redux'
import App from '../components/App'

const mapDispatchToProps = dispatch => { // eslint-disable-line
    return {
    }
}

const mapStateToProps = state => {
    return state
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
