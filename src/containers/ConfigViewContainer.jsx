import { connect } from 'react-redux'
import {
    addServer,
    removeServer,
    updateServer,
} from '../actions/index'
import ConfigView from '../components/ConfigView'

const mapDispatchToProps = dispatch => {
    return {
        addServer: server => dispatch(addServer(server)),
        removeServer: serverId => dispatch(removeServer(serverId)),
        updateServer: (serverId, server) => dispatch(updateServer(serverId, server))
    }
}

const mapStateToProps = state => {
    return { servers: state.servers }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfigView)
