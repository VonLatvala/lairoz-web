import { connect } from 'react-redux'
import { onHsvChange } from '../actions/index'
import StaticView from '../components/StaticView'

const mapDispatchToProps = dispatch => {
    return {
        onHsvChange: (h, s, v) => dispatch(onHsvChange(h, s, v))
    }
}

const mapStateToProps = (state, ownProps) => {
    const res = {
        ...ownProps,
        servers: state.servers,
        hsvConfig: state.hsvConfig,
    }
    return res
}

export default connect(mapStateToProps, mapDispatchToProps)(StaticView)
