import {
    ADD_SERVER,
    HSV_CHANGED,
    SET_VIEW,
    REMOVE_SERVER,
    UPDATE_SERVER,
    SET_ANIMATION_VIEW_SLIDERS,
    SET_ANIMATION_FROM_TO,
} from '../constants/action-types'


import newId from '../utils/newid'

// Persistent
const serversJson = localStorage.getItem('servers');
let servers = []
try {
    servers = JSON.parse(serversJson).map((server) => Object.assign({}, server, {serverId: newId('server')}))
    if(Array.isArray(servers))
        console.log('Loaded servers from localstorage', servers)
    else
        throw "Invalid servers"
} catch (err) {
    console.info('Got bad servers from localstorage, resetting')
    servers = []
}

const initialState = {
    appName: 'LairOZ Web',
    activeView: 'staticView',
    servers,
    hsvConfig: {
        h: 100,
        s: 100,
        v: 100
    },
    animatedConfig: {
        fromTo: {
            from: {
                h: 100,
                s: 100,
                v: 100
            },
            to: {
                h: 100,
                s: 100,
                v: 100
            },
        },
        speed: 3.5,
    },
};

function rootReducer(state = initialState, action) {

    if(action.type === SET_VIEW) {
        const activeView = action.viewId
        const res = Object.assign({}, state, {
            activeView
        })
        return res
    } else if(action.type === HSV_CHANGED) {
        const hsvConfig = action.payload
        const res = Object.assign({}, state, {
            hsvConfig,
        })
        return res
    } else if(action.type === ADD_SERVER) {
        const newServers = state.servers.concat(action.server)
        const res = Object.assign({}, state, {
            servers: newServers
        })
        localStorage.setItem('servers', JSON.stringify(newServers))
        return res
    } else if(action.type === REMOVE_SERVER) {
        const newServers = state.servers.filter((server) => {
            return action.serverId !== server.serverId
        })
        const res = Object.assign({}, state, {
            servers: newServers
        })
        localStorage.setItem('servers', JSON.stringify(newServers))
        return res
    } else if(action.type === UPDATE_SERVER) {
        const idxToChange = state.servers.findIndex((server) => server.serverId === action.serverId)
        const newServers = state.servers.slice(0)
        newServers[idxToChange] = action.server
        const res = Object.assign({}, state, {
            servers: newServers
        })
        localStorage.setItem('servers', JSON.stringify(newServers))
        return res
    } else if(action.type === SET_ANIMATION_VIEW_SLIDERS) {
        const res = Object.assign({}, state, {
            animatedConfig: {
                fromTo: action.fromTo,
                speed: action.speed,
            }
        })
        return res
    } else if(action.type === SET_ANIMATION_FROM_TO) {
        return state
    }
    return state;
}
export default rootReducer
