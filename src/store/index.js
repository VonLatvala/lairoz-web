import { createStore, applyMiddleware, } from "redux";

import promiseMiddleware from 'redux-promise-middleware';
import thunkMiddleware from 'redux-thunk';

import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from "../reducers/index";

const composedMiddleware = composeWithDevTools(
    applyMiddleware(
        thunkMiddleware,
        promiseMiddleware(),
    )
)

const store = createStore(
    rootReducer,
    composedMiddleware
);
export default store;
