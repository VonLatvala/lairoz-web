import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ServerStateSymbol from './ServerStateSymbol'

export default class ServerStateSymbolList extends Component {
    constructor(props) {
        super(props);
    }
    render() {

        let serverStateSymbols = this.props.servers.map((server) => {
            return (
                <ServerStateSymbol key={server.serverId} server={server} />
            )
        })

        return (
            [serverStateSymbols]
        );
    }
}

ServerStateSymbolList.propTypes = {
    servers: PropTypes.arrayOf(PropTypes.shape(
        {
            serverId: PropTypes.string.isRequired,
            address: PropTypes.string.isRequired,
            isEnabled: PropTypes.bool.isRequired,
            isWaiting: PropTypes.bool.isRequired,
            isError: PropTypes.bool.isRequired,
            hasConnected: PropTypes.bool.isRequired
        }
    ).isRequired).isRequired
};
