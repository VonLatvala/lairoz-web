import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AppNavContainer from '../containers/AppNavContainer';
import StaticViewContainer from '../containers/StaticViewContainer';
import AnimationViewContainer from '../containers/AnimationViewContainer'
import ConfigViewContainer from '../containers/ConfigViewContainer'
import '../css/style.css';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.congestionControl = {}

    }
    static propTypes = {
        servers: PropTypes.arrayOf(PropTypes.shape({
            serverId: PropTypes.string.isRequired,
            address: PropTypes.string.isRequired,
            isEnabled: PropTypes.bool.isRequired,
            isWaiting: PropTypes.bool.isRequired,
            isError: PropTypes.bool.isRequired,
            hasConnected: PropTypes.bool.isRequired,
        })),
        hsvConfig: PropTypes.shape({
            h: PropTypes.number.isRequired,
            s: PropTypes.number.isRequired,
            v: PropTypes.number.isRequired,
        }),
        appName: PropTypes.string.isRequired,
        activeView: PropTypes.string.isRequired,
    }

    views = {
        staticView: {
            title: 'Static'
        },
        animationView: {
            title: 'Animated'
        },
        configView: {
            title: 'Configuration'
        },
    }

    viewElems = {
        staticView: (
            <StaticViewContainer
                viewName='staticView'
                title='Static'
                key='staticView'
            />),
        animationView: (
            <AnimationViewContainer
                viewName='animationView'
                title='Animated'
                key='aimationView'
            />),
        configView: (
            <ConfigViewContainer
                viewName='configView'
                title='Configuration'
                key='configView'
            />
        )
    }
    render() {
        const navMappedViews = [];
        Object.keys(this.views).forEach((key) => {
            navMappedViews.push({ viewId: key, title: this.views[key].title });
        });
        const activeView = this.viewElems[this.props.activeView]
        return (
            <div>
                <div>
                    <AppNavContainer
                        views={ navMappedViews } />
                    <div className='container'>
                        { activeView }
                    </div>
                </div>
            </div>
        );
    }
}
