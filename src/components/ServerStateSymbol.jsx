import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ServerStateSymbol extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let statusIndicator = 'status-nc'
        if(this.props.server.isWaiting) {
            statusIndicator = 'status-waiting'
        } else if(this.props.server.isError) {
            statusIndicator = 'status-error'
        } else {
            statusIndicator = 'status-ok'
        }
        if(!this.props.server.isEnabled) {
            statusIndicator = 'status-nc'
        }
        return (
            <div className='col-xs-12 col-sm-2 col-lg-1'
                style={{
                    display: 'inline-block',
                    textAlign: 'center',
                    paddingLeft: '15px',
                    lineHeight: '38px',
                }}>
                <span className={'server-status-indicator '+statusIndicator}></span>
            </div>
        );
    }
}

ServerStateSymbol.propTypes = {
    server: PropTypes.shape(
        {
            serverId: PropTypes.string.isRequired,
            address: PropTypes.string.isRequired,
            isEnabled: PropTypes.bool.isRequired,
            isWaiting: PropTypes.bool.isRequired,
            isError: PropTypes.bool.isRequired,
            hasConnected: PropTypes.bool.isRequired
        }
    ).isRequired
};
