import React, { Component } from 'react';
import PropTypes from 'prop-types';
import StaticSliders from '../components/StaticSliders.jsx'
import ServerStateSymbolList from '../components/ServerStateSymbolList'

import 'rc-slider/assets/index.css';

export default class StaticView extends Component {
    constructor(props) {
        super(props);
    }
    static propTypes = {
        viewName: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        hsvConfig: PropTypes.shape({
            h: PropTypes.number.isRequired,
            s: PropTypes.number.isRequired,
            v: PropTypes.number.isRequired
        }),
        servers: PropTypes.arrayOf(PropTypes.shape(
            {
                serverId: PropTypes.string.isRequired,
                address: PropTypes.string.isRequired,
                isEnabled: PropTypes.bool.isRequired,
                isWaiting: PropTypes.bool.isRequired,
                isError: PropTypes.bool.isRequired,
                hasConnected: PropTypes.bool.isRequired
            }
        ).isRequired).isRequired,
        onHsvChange: PropTypes.func.isRequired
    };
    onSliderChange = (h, s, v) => {
        console.log("Sliders changed", h, s, v)
        this.props.onHsvChange(h, s, v)
    }
    render() {
        return (
            <div className='view'>
                <h2>{this.props.title}</h2>
                <hr />
                <StaticSliders
                    hsvConfig={this.props.hsvConfig}
                    onSliderChange={this.onSliderChange}
                />
                <hr />
                <ServerStateSymbolList servers={this.props.servers} />
            </div>
        );
    }
}


