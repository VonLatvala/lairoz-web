import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';

export default class AppNav extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    onClickNavItemHandler(viewId) {
        this.props.setView(viewId);
    }
    render() {
        const navElems = [];
        this.props.views.forEach((view) => {
            const itemKey = 'nav'+view.viewId;
            navElems.push(
                <NavItem key={itemKey}>
                    <NavLink className='linklike'
                        onClick={() => this.onClickNavItemHandler(view.viewId)}
                        active={this.props.activeView === view.viewId}
                    >
                        {view.title}
                    </NavLink>
                </NavItem>
            );
        });
        return (
            <div>
                <Navbar dark color="dark" fixed="top" expand="md">
                    <div className='container'>
                        <NavbarBrand href="/">{this.props.appName}</NavbarBrand>
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav navbar justified={false}>
                                {navElems}
                            </Nav>
                        </Collapse>
                    </div>
                </Navbar>
            </div>
        )
    }
}

AppNav.propTypes = {
    appName: PropTypes.string,
    views: PropTypes.array,
    setView: PropTypes.func.isRequired,
    activeView: PropTypes.string
};
