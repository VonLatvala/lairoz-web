import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../css/fancy-bootstrap-checkboxes.css';
import 'open-iconic/font/css/open-iconic-bootstrap.css';

export default class ServerConfigRow extends Component {
    constructor(props) {
        super(props);
        this.handleChangeEnabled = this.handleChangeEnabled.bind(this)
        this.uniqId = 'serverConfigRow'+this.props.server.serverId
    }

    static propTypes = {
        server: PropTypes.shape(
            {
                serverId: PropTypes.string.isRequired,
                address: PropTypes.string.isRequired,
                isEnabled: PropTypes.bool.isRequired,
                isWaiting: PropTypes.bool.isRequired,
                isError: PropTypes.bool.isRequired,
                hasConnected: PropTypes.bool.isRequired
            }
        ).isRequired,
        propagateData: PropTypes.func.isRequired,
        handleRemove: PropTypes.func.isRequired,
        updateServer: PropTypes.func.isRequired,
        deleteServer: PropTypes.func.isRequired,
    }

    handleChangeAddress = (newAddress) =>  {
        this.props.updateServer(this.props.server.serverId, Object.assign({}, this.props.server, {
            address: newAddress,
            isWaiting: false,
            isError: false,
        }))
    }

    handleChangeEnabled(isEnabled) {
        this.props.updateServer(this.props.server.serverId, Object.assign({}, this.props.server, {
            isEnabled,
            isWaiting: false,
            isError: false,
        }))
    }
    handleRemove() {
        this.props.handleRemove(this.props.server.serverId);
    }
    render() {
        let stateClassNames = ['btn'];
        if(!this.props.server.isEnabled) {
            stateClassNames.push('btn-secondary')
        } else if(this.props.server.isError) {
            stateClassNames.push('btn-danger')
        } else if(this.props.server.isWaiting) {
            stateClassNames.push('btn-warning')
        } else {
            stateClassNames.push('btn-success')
        }

        let statusIndicator = 'status-nc'
        if(this.props.server.isWaiting) {
            statusIndicator = 'status-waiting'
        } else if(this.props.server.isError) {
            statusIndicator = 'status-error'
        } else {
            statusIndicator = 'status-ok'
        }
        if(!this.props.server.isEnabled) {
            statusIndicator = 'status-nc'
        }

        return (
            <div
                className='serverConfigRow form-group'
                style={{
                    marginBottom: '16px'
                }}
            >
                <div className='col-xs-12 col-sm-2 col-lg-1'
                    style={{
                        display: 'inline-block',
                        textAlign: 'center',
                        paddingLeft: '15px',
                        lineHeight: '38px',
                        verticalAlign: 'middle',
                    }}>
                    <span className={'server-status-indicator '+statusIndicator}></span>
                </div>
                <div className='col-xs-12 col-sm-10 col-lg-11'
                    style={{
                        display: 'inline-block',
                        padding: 0,
                    }}>
                    <input
                        type='checkbox'
                        checked={ this.props.server.isEnabled }
                        onChange={ (e) => this.handleChangeEnabled(e.target.checked) }
                        name={ this.uniqId }
                        id={ this.uniqId }
                    />
                    <div className='btn-group'
                        style={{ width: '100%' }}>
                        <label
                            htmlFor={ this.uniqId }
                            className={ stateClassNames.join(' ') }
                            style={{ marginBottom: 0}}
                        >
                            <span className='oi oi-check'></span>
                            <span></span>
                        </label>
                        <input
                            type='text'
                            value={ this.props.server.address }
                            onChange={ (e) => this.handleChangeAddress(e.target.value) }
                            className=''
                            style={{ width: '100%'}}
                        />
                        <button
                            className='btn btn-danger'
                            onClick={this.handleRemove.bind(this)}
                        >
                            <span className='oi oi-trash' />
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

