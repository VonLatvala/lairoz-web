import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'rc-slider';
import dispatch from 'react-redux'; // eslint-disable-line
import ServerStateSymbolList from '../components/ServerStateSymbolList'
import { Button } from 'reactstrap';

export default class AnimatedView extends Component {
    constructor(props) {
        super(props);
    }


    static propTypes = {
        viewName: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        fromTo: PropTypes.shape({
            from: PropTypes.shape({
                h: PropTypes.number.isRequired,
                s: PropTypes.number.isRequired,
                v: PropTypes.number.isRequired,
            }),
            to: PropTypes.shape({
                h: PropTypes.number.isRequired,
                s: PropTypes.number.isRequired,
                v: PropTypes.number.isRequired,
            })
        }),
        speed: PropTypes.number.isRequired,
        servers: PropTypes.arrayOf(PropTypes.shape(
            {
                serverId: PropTypes.string.isRequired,
                address: PropTypes.string.isRequired,
                isEnabled: PropTypes.bool.isRequired,
                isWaiting: PropTypes.bool.isRequired,
                isError: PropTypes.bool.isRequired,
                hasConnected: PropTypes.bool.isRequired
            }
        ).isRequired).isRequired,
        onFrom: PropTypes.shape({
            onHueSliderChange: PropTypes.func.isRequired,
            onSaturationSliderChange: PropTypes.func.isRequired,
            onValueSliderChange: PropTypes.func.isRequired,
        }.isRequired).isRequired,
        onTo: PropTypes.shape({
            onHueSliderChange: PropTypes.func.isRequired,
            onSaturationSliderChange: PropTypes.func.isRequired,
            onValueSliderChange: PropTypes.func.isRequired,
        }.isRequired).isRequired,
        onSpeedChange: PropTypes.func.isRequired,
        onCommitAnimation: PropTypes.func.isRequired,
    };

    render() {
        const sliderMin = 0;
        const sliderMax = 100;
        const step = 0.01;
        return (
            <div className='view'>
                <h2>{this.props.title}</h2>
                <hr />
                <h3>From</h3>
                <img src='img/hue.png' style={{
                    width: '100%',
                    marginBottom: '8px'
                }} />
                <Slider
                    min={sliderMin}
                    max={sliderMax}
                    step={step}
                    value={this.props.fromTo.from.h}
                    onChange={(val) => { this.props.onFrom.onHueSliderChange(val, this.props.fromTo, this.props.speed)}}
                    style={{ marginBottom: '16px'}}
                />
                <p style={{
                    margin: '0 auto',
                }}>Saturation</p>
                <Slider
                    min={sliderMin}
                    max={sliderMax}
                    step={step}
                    value={this.props.fromTo.from.s}
                    onChange={(val) => { this.props.onFrom.onSaturationSliderChange(val, this.props.fromTo, this.props.speed)}}
                    style={{ marginBottom: '16px'}}
                />
                <p style={{
                    margin: '0 auto',
                }}>Value</p>
                <Slider
                    min={sliderMin}
                    max={sliderMax}
                    step={step}
                    value={this.props.fromTo.from.v}
                    onChange={(val) => { this.props.onFrom.onValueSliderChange(val, this.props.fromTo, this.props.speed)}}
                    style={{ marginBottom: '16px'}}
                />
                <hr />
                <h3>To</h3>
                <img src='img/hue.png' style={{
                    width: '100%',
                    marginBottom: '8px'
                }} />
                <Slider
                    min={sliderMin}
                    max={sliderMax}
                    step={step}
                    value={this.props.fromTo.to.h}
                    onChange={(val) => { this.props.onTo.onHueSliderChange(val, this.props.fromTo, this.props.speed)}}
                    style={{ marginBottom: '16px'}}
                />
                <p style={{
                    margin: '0 auto',
                }}>Saturation</p>
                <Slider
                    min={sliderMin}
                    max={sliderMax}
                    step={step}
                    value={this.props.fromTo.to.s}
                    onChange={(val) => { this.props.onTo.onSaturationSliderChange(val, this.props.fromTo, this.props.speed)}}
                    style={{ marginBottom: '16px'}}
                />
                <p style={{
                    margin: '0 auto',
                }}>Value</p>
                <Slider
                    min={sliderMin}
                    max={sliderMax}
                    step={step}
                    value={this.props.fromTo.to.v}
                    onChange={(val) => { this.props.onTo.onValueSliderChange(val, this.props.fromTo, this.props.speed)}}
                    style={{ marginBottom: '16px'}}
                />
                <h3>Speed</h3>
                <Slider
                    min={1}
                    max={6}
                    step={step}
                    value={this.props.speed}
                    onChange={(val) => { this.props.onSpeedChange(val, this.props.fromTo)}}
                    style={{ marginBottom: '16px'}}
                />
                <hr />
                <ServerStateSymbolList servers={this.props.servers} />
                <hr />
                <div style={{textAlign: 'right'}}>
                    <Button onClick={ () => this.props.onCommitAnimation(this.props.fromTo, this.props.speed) } color="primary">Set</Button>
                </div>
            </div>
        );
    }
}


