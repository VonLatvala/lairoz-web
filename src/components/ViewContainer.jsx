import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ViewContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const ActiveView = this.props.views[this.props.activeView].el;
        return (
            <div className='container'>
                {ActiveView}
            </div>
        );
    }
}

ViewContainer.propTypes = {
    activeView: PropTypes.string.isRequired,
    views: PropTypes.object.isRequired, // XXX: please specify what kind of object
}
