import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'rc-slider';

export default class StaticSliders extends Component {
    constructor(props) {
        super(props);
    }
    onHueSliderChange(value) {
        // console.log('Changed hue slider', value)
        this.props.onSliderChange(value,
            this.props.hsvConfig.s, this.props.hsvConfig.v)
    }
    onSaturationSliderChange(value) {
        // console.log('Changed saturation slider', value)
        this.props.onSliderChange(this.props.hsvConfig.h,
            value, this.props.hsvConfig.v)
    }
    onValueSliderChange(value) {
        // console.log('Changed value slider', value)
        this.props.onSliderChange(this.props.hsvConfig.h,
            this.props.hsvConfig.s, value)
    }
    render() {
        const sliderMin = 0;
        const sliderMax = 100;
        const step = 0.01;
        // const hsv = this.props.hsvConfig
        // console.log('Rendering sliders with hue', hsv.h, 'saturation', hsv.s, 'value', hsv.v)
        return (
            <div className='container'>
                <img src='img/hue.png' style={{
                    width: '100%',
                    marginBottom: '8px'
                }} />
                <Slider
                    min={sliderMin}
                    max={sliderMax}
                    step={step}
                    value={this.props.hsvConfig.h}
                    onChange={this.onHueSliderChange.bind(this)}
                    style={{ marginBottom: '16px'}}
                />
                <p style={{
                    margin: '0 auto',
                }}>Saturation</p>
                <Slider
                    min={sliderMin}
                    max={sliderMax}
                    step={step}
                    value={this.props.hsvConfig.s}
                    onChange={this.onSaturationSliderChange.bind(this)}
                    style={{ marginBottom: '16px'}}
                />
                <p style={{
                    margin: '0 auto',
                }}>Value</p>
                <Slider
                    min={sliderMin}
                    max={sliderMax}
                    step={step}
                    value={this.props.hsvConfig.v}
                    onChange={this.onValueSliderChange.bind(this)}
                    style={{ marginBottom: '16px'}}
                />
            </div>
        )
    }
}

StaticSliders.propTypes = {
    onSliderChange: PropTypes.func.isRequired,
    hsvConfig: PropTypes.shape({
        h: PropTypes.number.isRequired,
        s: PropTypes.number.isRequired,
        v: PropTypes.number.isRequired,
    })
};
