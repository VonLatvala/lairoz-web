import React, { Component } from 'react';
import PropTypes from 'prop-types';
import newId from '../utils/newid'
import ServerConfigRowContainer from '../containers/ServerConfigRowContainer';

export default class ConfigView extends Component {
    constructor(props) {
        super(props);
        this.handleRemoveRow = this.handleRemoveRow.bind(this)
    }

    static propTypes = {
        viewName: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        servers: PropTypes.arrayOf(PropTypes.shape(
            {
                address: PropTypes.string.isRequired,
                isEnabled: PropTypes.bool.isRequired,
                isWaiting: PropTypes.bool.isRequired,
                isError: PropTypes.bool.isRequired,
                hasConnected: PropTypes.bool.isRequired
            }
        )).isRequired,
        addServer: PropTypes.func.isRequired,
        removeServer: PropTypes.func.isRequired,
    }

    handleAddRow() {
        this.props.addServer(this.createNewServer())
    }
    createNewServer() {
        const newServer = {
            serverId: newId('server'),
            address: `wifirgb.local`,
            isEnabled: true,
            isWaiting: false,
            isError: false,
            hasConnected: false,
        }
        return newServer
    }
    handleRemoveRow(serverId) {
        this.props.removeServer(serverId)
    }
    render() {
        const serverConfigRowElems = [];
        // FIXME: This is retardedly implemented
        let servers = this.props.servers
        servers.forEach((server, idx) => {
            const itemKey = 'serverConfigRow'+server.serverId
            const onChange = (data) => {
                const oldServers = this.props.servers
                const newServer = oldServers[idx]
                newServer.address = data.server.address
                newServer.isEnabled = data.server.isEnabled
                const newServers = []
                oldServers.forEach((el2, idx2) => {
                    if(idx === idx2)
                        newServers.push(newServer)
                    else
                        newServers.push(el2)
                })
            };
            serverConfigRowElems.push(
                <ServerConfigRowContainer
                    key={itemKey}
                    server={server}
                    propagateData={onChange}
                    handleRemove={this.handleRemoveRow}
                />
            );
        })

        return (
            <div className='view'>
                <h2>{this.props.title}</h2>
                <hr />
                { serverConfigRowElems }
                <button
                    className='btn btn-success'
                    style={{ width: '100%' }}
                    onClick={this.handleAddRow.bind(this)}
                >
                    <span className='oi oi-plus' />
                </button>
                <hr />
            </div>
        );
    }
}
