import {
    ADD_SERVER,
    HSV_CHANGED,
    SET_VIEW,
    REMOVE_SERVER,
    UPDATE_SERVER,
    SET_ANIMATION_VIEW_SLIDERS,
} from '../constants/action-types'

import {
    RGB_MODE_STATIC,
    RGB_MODE_ANIMATED_FROM_TO,
} from '../constants/rgb-modes'

import RgbController from '../utils/RgbController'

export function setView(viewId) {
    return {
        type: SET_VIEW,
        viewId
    }
}

export function addServer(server) {
    return {
        type: ADD_SERVER,
        server
    }
}


export function removeServer(serverId) {
    return {
        type: REMOVE_SERVER,
        serverId
    }

}

export function updateServer(serverId, server) {
    console.log('Running updateServer with id', serverId)
    return {
        type: UPDATE_SERVER,
        serverId,
        server
    }

}

export function onHsvChange(h, s, v) {
    return (dispatch) => {
        console.log('Setting RGB for ALL servers')
        dispatch(setRgbForAllServers({
            mode: RGB_MODE_STATIC,
            hsvConfig: {h, s, v}
        }))
        dispatch({
            type: HSV_CHANGED,
            payload: {h, s, v}
        })
    }
}

export function setRgbForAllServers(payload) {
    return (dispatch, getState) => {
        getState().servers.filter((server) => server.isEnabled && !server.isWaiting)
            .forEach((server) => {
                dispatch(setRgb(payload, server))
            })
    }
}

export function setRgb(rgbOpts, server) {
    return (dispatch) => {
        if(rgbOpts.mode === RGB_MODE_STATIC) {
            console.log('Setting static', rgbOpts.hsvConfig, server)
            RgbController.setStatic(rgbOpts.hsvConfig, server).then((res) => {
                console.log('Sat static for', server, 'and got a response', res)
                console.log('Setting SUCCESS on server', server.serverId)
                dispatch(updateServer(server.serverId, {...server, isWaiting: false, isError: false}))
            }).catch((err) => {
                console.error('Couldn\'t set static for', server, 'because', err)
                console.log('Setting ERROR on server', server.serverId)
                dispatch(updateServer(server.serverId, {...server, isWaiting: false, isError: true}))
            })
            console.log('Setting WAIT on server', server.serverId)
            dispatch(updateServer(server.serverId, {...server, isWaiting: true, isError: false}))
        } else if(rgbOpts.mode === RGB_MODE_ANIMATED_FROM_TO) {
            console.log('Setting animated breathe', rgbOpts.fromTo, rgbOpts.speed, server)
            RgbController.setAnimationFromTo(rgbOpts.fromTo, rgbOpts.speed, server).then((res) => {
                console.log('Sat animation breathe', server, 'and got a response', res)
                console.log('Setting SUCCESS on server', server.serverId)
                dispatch(updateServer(server.serverId, {...server, isWaiting: false, isError: false}))
            }).catch((err) => {
                console.error('Couldn\'t set animation breathe for', server, 'because', err)
                console.log('Setting ERROR on server', server.serverId)
                dispatch(updateServer(server.serverId, {...server, isWaiting: false, isError: true}))
            })
            console.log('Setting WAIT on server', server.serverId)
            dispatch(updateServer(server.serverId, {...server, isWaiting: true, isError: false}))
        }
    }
}

export function setAnimatedViewSliders(from, to, speed) {
    return (dispatch) => {
        dispatch({
            type: SET_ANIMATION_VIEW_SLIDERS,
            fromTo: {
                from,
                to
            },
            speed
        })

    }
}

export function setAnimatedFromTo(fromTo, speed) {
    return (dispatch) => {
        dispatch(setRgbForAllServers({
            mode: RGB_MODE_ANIMATED_FROM_TO,
            fromTo,
            speed
        }))
    }
}
