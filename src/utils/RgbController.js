import axios from 'axios';
import Promise from 'es6-promise';
import { hsv2Rgb, rgb2Hsv } from '../utils/Color'; // eslint-disable-line
import { RGB_COMMAND_TIMEOUT_MS } from '../constants/defaults'

class RgbController {
    constructor() {
        this.timeoutMs = RGB_COMMAND_TIMEOUT_MS;

    }

    congestionControl = []

    setStatic(hsvConfig, server) {
        return new Promise((resolve, reject) => {
            const rgbFromHsv = hsv2Rgb(hsvConfig.h, hsvConfig.s, hsvConfig.v)
            const buildReqUrl = (server, r, g, b) => {
                return `http://${server.address}/api/setColor?channelR=${r/100}&channelG=${g/100}&channelB=${b/100}`
            }
            // console.log('Sending request to put HSV to', rgbFromHsv)
            let reqUrl = buildReqUrl(server, rgbFromHsv.r, rgbFromHsv.g, rgbFromHsv.b)
            // console.log('Requesting', reqUrl)
            if(typeof this.congestionControl[server.serverId] === 'undefined')
                this.congestionControl[server.serverId] = false
            if(this.congestionControl[server.serverId] === false) {
                axios.get(reqUrl, {timeout: this.timeoutMs})
                    .then(() => {
                        this.congestionControl[server.serverId] = false
                        console.log(`Successfully set server ${server.address} to do color (`,
                            `H${hsvConfig.h}`,
                            `S${hsvConfig.s}`,
                            `V${hsvConfig.v}`,`)`)
                        resolve()
                    }).catch((err) => {
                        if(server !== {}) {
                            console.error('nyet', err)
                            this.congestionControl[server.serverId] = false
                            reject(err)
                        }
                    })
                this.congestionControl[server.serverId] = true
            } else {
                console.log('Congestion control for server', server.address)
                resolve()
            }
        })
    }
    setAnimationFromTo(fromTo, speed, server) {
        const sliderToVal = (x) => ((100000000.0)/(Math.pow(x+1.0, 6)))+100.6
        return new Promise((resolve, reject) => {
            const fromRgbFromHsv = hsv2Rgb(fromTo.from.h, fromTo.from.s, fromTo.from.v)
            const toRgbFromHsv = hsv2Rgb(fromTo.to.h, fromTo.to.s, fromTo.to.v)
            console.log(fromRgbFromHsv, toRgbFromHsv)
            const buildReqUrl = (server, fromRgbFromHsv, toRgbFromHsv, speed) => {
                return `http://${server.address}/api/animation/breathe?color1R=${fromRgbFromHsv.r/100}&color1G=${fromRgbFromHsv.g/100}&color1B=${fromRgbFromHsv.b/100}&color2R=${toRgbFromHsv.r/100}&color2G=${toRgbFromHsv.g/100}&color2B=${toRgbFromHsv.b/100}&speed=${sliderToVal(speed)}`
            }
            let reqUrl = buildReqUrl(server, fromRgbFromHsv, toRgbFromHsv, speed)
            if(typeof this.congestionControl[server.serverId] === 'undefined')
                this.congestionControl[server.serverId] = false
            if(this.congestionControl[server.serverId] === false) {
                axios.get(reqUrl, {timeout: this.timeoutMs})
                    .then(() => {
                        this.congestionControl[server.serverId] = false
                        console.log(`Successfully set server ${server.address} to do animation breathe (`,
                            `H${fromTo.from.h/100}`,
                            `S${fromTo.from.s/100}`,
                            `V${fromTo.from.v/100}`,`) -> (`,
                            `H${fromTo.to.h/100}`,
                            `S${fromTo.to.s/100}`,
                            `V${fromTo.to.v/100}`,`) @ ${speed}`)
                        resolve()
                    }).catch((err) => {
                        if(server !== {}) {
                            console.error('nyet', err)
                            this.congestionControl[server.serverId] = false
                            reject(err)
                        }
                    })
                this.congestionControl[server.serverId] = true
            } else {
                console.log('Congestion control for server', server.address)
                resolve()
            }
        })
    }
}

export default new RgbController()
