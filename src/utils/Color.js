/**
 * Converts an RGB color value to HSV. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and v in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSV representation
 */
function rgbToHsv(r, g, b) {
    // console.log('rgbToHsv got as input r', r, 'g', g, 'b', b)
    r /= 255, g /= 255, b /= 255;
    // console.log('after conversion, r', r, 'g', g, 'b', b)

    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, v = max;

    var d = max - min;
    s = max == 0 ? 0 : d / max;

    if (max == min) {
        h = 0; // achromatic
    } else {
        switch (max) {
        case r: h = (g - b) / d + (g < b ? 6 : 0); break;
        case g: h = (b - r) / d + 2; break;
        case b: h = (r - g) / d + 4; break;
        }

        h /= 6;
    }
    // console.log('and as a result, h', h, 's', s, 'v', v)

    return [ h, s, v ];
}

/**
 * Converts an HSV color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes h, s, and v are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   Number  h       The hue
 * @param   Number  s       The saturation
 * @param   Number  v       The value
 * @return  Array           The RGB representation
 */
function hsvToRgb(h, s, v) {
    // console.log('hsvToRgb got as input h', h, 's', s, 'v', v)
    var r, g, b;

    var i = Math.floor(h * 6);
    var f = h * 6 - i;
    var p = v * (1 - s);
    var q = v * (1 - f * s);
    var t = v * (1 - (1 - f) * s);

    switch (i % 6) {
    case 0: r = v, g = t, b = p; break;
    case 1: r = q, g = v, b = p; break;
    case 2: r = p, g = v, b = t; break;
    case 3: r = p, g = q, b = v; break;
    case 4: r = t, g = p, b = v; break;
    case 5: r = v, g = p, b = q; break;
    }

    // console.log('and as a result, r', r*255, 'g', g*255, 'b', b*255)
    return [ r * 255, g * 255, b * 255 ];
}

/**
 * Wrapper function for hsvToRgb.
 * Assumes h, s and v are contained in the set [0, 100] and
 * returns r, g and b in the set [0, 100]
 */
export function hsv2Rgb(h, s, v) {
    // console.log('hsv2Rgb got as input h', h, 's', s, 'v', v)
    const intermediary = hsvToRgb(h / 100, s / 100, v / 100)
    // console.log('hsv2Rgb after conversion', intermediary)
    // console.log('Resulting in', {
    // r: (intermediary[0] / 255) * 100 || 0,
    // g: (intermediary[1] / 255) * 100 || 0,
    // b: (intermediary[2] / 255) * 100 || 0,
    // })
    return {
        r: (intermediary[0] / 255) * 100 || 0,
        g: (intermediary[1] / 255) * 100 || 0,
        b: (intermediary[2] / 255) * 100 || 0,
    }
}

/**
 * Wrapper function for rgbToHsv§.
 * Assumes h, s and v are contained in the set [0, 100] and
 * returns r, g and b in the set [0, 100]
 */
export function rgb2Hsv(h, s, v) {
    const intermediary = rgbToHsv((h / 255) * 100 || 0, (s / 255) * 100 || 0, (v / 255) * 100 || 0)
    return {
        h: intermediary[0] * 100,
        s: intermediary[1] * 100,
        v: intermediary[2] * 100,
    }
}
