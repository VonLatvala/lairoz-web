import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import { Provider } from 'react-redux'
import store from './store'
import AppContainer from './containers/AppContainer';

ReactDOM.render(
    (
        <Provider store={store}>
            <AppContainer />
        </Provider>
    ), document.getElementById('app')
)
